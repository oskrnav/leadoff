=== WooCommerce SecureNet Gateway ===
Author: skyverge
Tags: woocommerce
Requires at least: 3.8
Tested up to: 4.1
Requires WooCommerce at least: 2.1
Tested WooCommerce up to: 2.3

Accept Credit Cards via SecureNet in your WooCommerce store

See http://docs.woothemes.com/document/securenet-payment-gateway/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-gateway-securenet' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
