<?php
/**
 * Plugin Name: WooCommerce SecureNet Gateway
 * Plugin URI: http://www.woothemes.com/products/securenet-gateway/
 * Description: Extends WooCommerce with a <a href="https://www.securenet.com" target="_blank">SecureNet</a> gateway. A SecureNet gateway account, and a server with SSL support and an SSL certificate is required for security reasons.
 * Author: SkyVerge
 * Author URI: http://www.skyverge.com
 * Version: 1.3.0
 * Text Domain: wc-gateway-securenet
 * Domain Path: /i18n/languages/
 *
 * Copyright: (c) 2014-2015 SkyVerge, Inc.
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package   WC-Gateway-SecureNet
 * @author    SkyVerge
 * @category  Gateways
 * @copyright Copyright (c) 2015, SkyVerge, Inc.
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) )
	require_once( 'woo-includes/woo-functions.php' );

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), '0342525bd00aec14aa0f39a1e4c7676d', '18713' );

add_action('plugins_loaded', 'woocommerce_gateway_securenet_init', 0);

function woocommerce_gateway_securenet_init() {

	if ( ! class_exists( 'WC_Payment_Gateway' ) ) return;

	/**
	 * Localization
	 */
	load_plugin_textdomain( 'wc-gateway-securenet', false, dirname( plugin_basename( __FILE__ ) ) . '/i18n/languages' );

	define('SECURENET_DIR', WP_PLUGIN_DIR . "/" . plugin_basename( dirname(__FILE__) ) . '/');

	/**
	 * SecureNet Gateway Class
	 **/
	class WC_Gateway_Securenet extends WC_Payment_Gateway {

		public function __construct() {

			$this->id   				= 'securenet';
			$this->method_title 		= __( 'SecureNet', 'wc-gateway-securenet' );
			$this->method_description 	= __( 'SecureNet allows customers to checkout using a credit card', 'wc-gateway-securenet', 'wc-gateway-securenet');
			$this->logo 				= untrailingslashit( plugins_url( '/', __FILE__ ) ) . '/images/securenet.jpg';
			$this->icon   				= untrailingslashit( plugins_url( '/', __FILE__ ) ) . '/images/cards.png';
			$this->has_fields  			= false;
			$this->livegatewayurl 		= "https://gateway.securenet.com/API/Gateway.svc/webHttp/ProcessTransaction";
			$this->testgatewayurl 		= "https://certify.securenet.com/API/Gateway.svc/webHttp/ProcessTransaction";

			// Load the form fields
			$this->init_form_fields();

			// Load the settings.
			$this->init_settings();

			// Get setting values
			$this->enabled    		= $this->settings['enabled'];
			$this->title    		= $this->settings['title'];
			$this->description  	= $this->settings['description'];
			$this->securenet_id  	= $this->settings['securenet_id'];
			$this->securenet_key  	= $this->settings['securenet_key'];
			$this->command  		= $this->settings['command'];
			$this->emailreceipt		= $this->settings['emailreceipt'];
			$this->testmode   		= $this->settings['testmode'];
			$this->testurl			= $this->settings['testurl'];
			$this->debugon   		= $this->settings['debugon'];
			$this->debugrecipient  	= $this->settings['debugrecipient'];

			// Actions
			add_action('admin_notices', array(&$this, 'securenet_ssl_check'));
			// Check if this is version 1.x.x of WooCommerce
			add_action('woocommerce_update_options_payment_gateways', array( $this, 'process_admin_options'));
			add_action('woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options'));
		}


		/**
		 * Check if SSL is enabled and notify the user
		 */
		function securenet_ssl_check() {

			if (get_option('woocommerce_force_ssl_checkout')=='no' && $this->enabled=='yes') :

				echo '<div class="error"><p>'.sprintf(__('SecureNet is enabled and the <a href="%s">Force secure checkout</a> option is disabled; your checkout is not secure! Please enable this feature and ensure your server has a valid SSL certificate installed.', 'wc-gateway-securenet'), admin_url('admin.php?page=woocommerce')).'</p></div>';

			endif;
		}


		/**
		 * Initialize Gateway Settings Form Fields
		 */
		function init_form_fields() {

			$this->form_fields = array(
				'enabled' => array(
					'title' => __( 'Enable/Disable', 'wc-gateway-securenet' ),
					'label' => __( 'Enable SecureNet Gateway', 'wc-gateway-securenet' ),
					'type' => 'checkbox',
					'description' => '',
					'default' => 'no'
				),
				'title' => array(
					'title' => __( 'Title', 'wc-gateway-securenet' ),
					'type' => 'text',
					'description' => __( 'This controls the title which the user sees during checkout.', 'wc-gateway-securenet' ),
					'default' => __( 'Credit Card (SecureNet)', 'wc-gateway-securenet' )
				),
				'description' => array(
					'title' => __( 'Description', 'wc-gateway-securenet' ),
					'type' => 'textarea',
					'description' => __( 'This controls the description which is displayed to the customer.', 'wc-gateway-securenet' ),
					'default' => 'Pay with your MasterCard, Visa, Discover or American Express'
				),
				'securenet_id' => array(
					'title' => __( 'SecureNet ID', 'wc-gateway-securenet' ),
					'type' => 'text',
					'description' => __( 'Your site\'s SecureNet ID - visible at the top of the Virtual Terminal system.', 'wc-gateway-securenet' ),
					'default' => ''
				),
				'securenet_key' => array(
					'title' => __( 'SecureNet Key', 'wc-gateway-securenet' ),
					'type' => 'text',
					'description' => __( 'Your site\'s SecureNet Key - Retrieve from the Virtual Terminal Settings section.', 'wc-gateway-securenet' ),
					'default' => ''
				),
				'command' => array(
					'title' => __( 'Payment Type', 'wc-gateway-securenet' ),
					'type' => 'select',
					'description' => __( 'Payment command to run. ', 'wc-gateway-securenet' ),
					'options' => array('0100'=>'Authorize and Capture',
									   '0000'=>'Authorize Only'),
					'default' => ''
				),
				'emailreceipt' => array(
					'title' => __( 'SecureNet Email Receipt', 'wc-gateway-securenet' ),
					'label' => __( 'Enable SecureNet receipt email', 'wc-gateway-securenet' ),
					'type' => 'checkbox',
					'description' => __( 'If checked the customer will receive a receipt of the transaction from SecureNet to their billing email address.', 'wc-gateway-securenet' ),
					'default' => 'no'
				),
				'testmode' => array(
					'title' => __( 'Test Mode', 'wc-gateway-securenet' ),
					'label' => __( 'Enable Test Mode', 'wc-gateway-securenet' ),
					'type' => 'checkbox',
					'description' => __( 'If checked then the transaction will be sent with Test flag set to true.', 'wc-gateway-securenet' ),
					'default' => 'no'
				),
				'testurl' => array(
					'title' => __( 'Test URL', 'wc-gateway-securenet' ),
					'label' => __( 'Enable Test URL', 'wc-gateway-securenet' ),
					'type' => 'checkbox',
					'description' => __( 'If checked then the transaction will be sent to the SecureNet test URL.', 'wc-gateway-securenet' ),
					'default' => 'no'
				),
				'debugon' => array(
					'title' => __( 'Debugging', 'wc-gateway-securenet' ),
					'label' => __( 'Enable debug emails', 'wc-gateway-securenet' ),
					'type' => 'checkbox',
					'description' => __( 'Receive emails containing the data sent to and from SecureNet.', 'wc-gateway-securenet' ),
					'default' => 'no'
				),
				'debugrecipient' => array(
					'title' => __( 'Debugging Email', 'wc-gateway-securenet' ),
					'type' => 'text',
					'description' => __( 'Who should receive the debugging emails.', 'wc-gateway-securenet' ),
					'default' =>  get_option('admin_email')
				),
			);
		}


		/**
		 * Admin Panel Options
		 * - Options for bits like 'title' and availability on a country-by-country basis
		 */
		public function admin_options() {
?>
			<p><a href="http://www.securenet.com/" target="_blank"><img src="<?php echo $this->logo;?>" /></a></p>
			<h3><?php _e('SecureNet', 'wc-gateway-securenet'); ?></h3>
	    	<p><?php _e( 'SecureNet allows customers to checkout using a credit card by adding credit card fields on the checkout page and then sending the details to SecureNet for verification.', 'wc-gateway-securenet' ); ?></p>
	    	<table class="form-table">
	    		<?php $this->generate_settings_html(); ?>
			</table><!--/.form-table-->
	    	<?php
		}


		/**
		 * Payment fields for SecureNet.
		 */
		function payment_fields() {
			?>
			<fieldset>
				<p class="form-row form-row-first">
					<label for="securenet_ccnum"><?php echo __("Credit card number", 'wc-gateway-securenet') ?> <span class="required">*</span></label>
					<input type="text" class="input-text" id="securenet_ccnum" name="securenet_ccnum" />
				</p>
				<div class="clear"></div>

				<p class="form-row form-row-first">
					<label for="securenet_expmonth"><?php echo __("Expiration date", 'wc-gateway-securenet') ?> <span class="required">*</span></label>
					<select name="securenet_expmonth" id="securenet_expmonth" class="woocommerce-select woocommerce-cc-month">
						<option value=""><?php _e('Month', 'wc-gateway-securenet') ?></option>
						<?php
							$months = array();
							for ($i = 1; $i <= 12; $i++) {
								$timestamp = mktime(0, 0, 0, $i, 1);
								$months[date('m', $timestamp)] = date('F', $timestamp);
							}
							foreach ($months as $num => $name) {
								printf('<option value="%s">%s</option>', $num, $name);
							}
						?>
					</select>
					<select name="securenet_expyear" id="securenet_expyear" class="woocommerce-select woocommerce-cc-year">
						<option value=""><?php _e('Year', 'wc-gateway-securenet') ?></option>
						<?php
							$years = array();
							for ($i = date('y'); $i <= date('y') + 15; $i++) {
								printf('<option value="%u">20%u</option>', $i, $i);
							}
						?>
					</select>
				</p>

				<p class="form-row form-row-last">
					<label for="securenet_cvv"><?php _e("Card security code", 'wc-gateway-securenet') ?> <span class="required">*</span></label>
					<input type="text" class="input-text" id="securenet_cvv" name="securenet_cvv" maxlength="4" style="width:45px" />
				</p>

				<div class="clear"></div>
				<p><?php echo $this->description ?></p>
			</fieldset>
			<?php
		}


		/**
		 * Process the payment and return the result
		 **/

		function process_payment( $order_id ) {

			$order = new WC_Order( $order_id );
			$testmode = ( $this->testmode == 'yes') ? 'TRUE' : 'FALSE' ;
			$gatewayurl = ( $this->testurl == 'yes') ? $this->testgatewayurl : $this->livegatewayurl;
			$emailreceipt = ( $this->emailreceipt == 'yes' ) ? 'TRUE' : 'FALSE' ;

			// Create request
			$request['AMOUNT'] 							= $order->order_total;
			$request['CARD']['CARDCODE'] 				= $this->get_post('securenet_cvv');
			$request['CARD']['CARDNUMBER'] 				= $this->get_post('securenet_ccnum');
			$request['CARD']['EXPDATE'] 				= $this->get_post('securenet_expmonth') . $this->get_post('securenet_expyear');
			$request['CODE'] 							= $this->command;
			$request['CUSTOMERIP']						= $_SERVER['REMOTE_ADDR'];
			$request['CUSTOMERID']						= $order->user_id;
			$request['CUSTOMER_BILL']['ADDRESS']		= $order->billing_address_1;
			$request['CUSTOMER_BILL']['CITY']			= $order->billing_city;
			$request['CUSTOMER_BILL']['COMPANY']		= $order->billing_company;
			$request['CUSTOMER_BILL']['COUNTRY']		= $order->billing_country;
			$request['CUSTOMER_BILL']['EMAIL']			= $order->billing_email;
			$request['CUSTOMER_BILL']['EMAILRECEIPT']	= $emailreceipt;
			$request['CUSTOMER_BILL']['FIRSTNAME']		= $order->billing_first_name;
			$request['CUSTOMER_BILL']['LASTNAME']		= $order->billing_last_name;
			$request['CUSTOMER_BILL']['PHONE']			= $order->billing_phone;
			$request['CUSTOMER_BILL']['STATE']			= $order->billing_state;
			$request['CUSTOMER_BILL']['ZIP']			= $order->billing_postcode;
			$request['CUSTOMER_SHIP']['ADDRESS']		= $order->shipping_address_1;
			$request['CUSTOMER_SHIP']['CITY']			= $order->shipping_city;
			$request['CUSTOMER_SHIP']['COMPANY']		= $order->shipping_company;
			$request['CUSTOMER_SHIP']['COUNTRY']		= $order->shipping_country;
			$request['CUSTOMER_SHIP']['FIRSTNAME']		= $order->shipping_first_name;
			$request['CUSTOMER_SHIP']['LASTNAME']		= $order->shipping_last_name;
			$request['CUSTOMER_SHIP']['STATE']			= $order->shipping_state;
			$request['CUSTOMER_SHIP']['ZIP']			= $order->shipping_postcode;
			$request['DCI'] 							= "0";
			$request['INSTALLMENT_SEQUENCENUM'] 		= "0";
			$request['MERCHANT_KEY']['GROUPID'] 		= "0";
			$request['MERCHANT_KEY']['SECUREKEY'] 		= $this->securenet_key;
			$request['MERCHANT_KEY']['SECURENETID'] 	= $this->securenet_id;
			$request['METHOD'] 							= "CC";
			$request['ORDERID'] 						= $order->id;
			$request['OVERRIDE_FROM'] 					= "0";
			$request['RETAIL_LANENUM'] 					= "0";
			$request['TEST']							= $testmode;
			$request['TOTAL_INSTALLMENTCOUNT'] 			= "0";
			$request['TRANSACTION_SERVICE'] 			= "0";
			$request['ENCRYPTION']['ENCRYPTIONMODE']	= '0';

			// Don't send card data via email
			$debug_request = $request;
			$debug_request['CARD']['CARDCODE'] 			= 'XXX';
			$debug_request['CARD']['CARDNUMBER'] 		= 'XXXXXXXXXXXXXXXX';
			$debug_request['CARD']['EXPDATE'] 			= 'XXXX';

			$xml  = '<TRANSACTION xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://gateway.securenet.com/API/Contracts">';
			$xml .= $this->xmlize($request);

			$xml .= '<DEVELOPERID>10000081</DEVELOPERID>';
			$xml .= '<VERSION>4.13</VERSION>';
			$xml .= '</TRANSACTION>';


			$this->send_debugging_email( "SecureNet Gateway Request: \nGateway: " . $gatewayurl . "\n\n". print_r($debug_request, true));

			$this->send_debugging_email( "SecureNet Gateway Request XML: \nGateway: " . $gatewayurl . "\n\n". $xml);


			// ************************************************
			// Process request


			$header[] = "Content-type: text/xml";
			$header[] = "Content-length: ".strlen($xml) . "\r\n";
			$header[] = $xml;

			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $gatewayurl);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, $header );
			curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
			$response = curl_exec( $ch );
			curl_close($ch);


			// ************************************************
			// Retreive response

			$response_object = simplexml_load_string($response);

			$this->send_debugging_email( "SecureNet Gateway Response: \n\nRESPONSE:\n"
										. print_r($response,true)
										. "\n\nDATA:\n". print_r($response_object,true));


			if ($response_object->TRANSACTIONRESPONSE->RESPONSE_CODE == 1) {
				// Successful payment or authorization

				if ( $response_object->TRANSACTIONRESPONSE->CODE == '0000') {
					$trans_message = __('SecureNet authorization complete. Complete payment capture in your SecureNet dashboard. ', 'wc-gateway-securenet');
				} elseif ( $response_object->TRANSACTIONRESPONSE->CODE == '0100') {
					$trans_message = __('SecureNet authorization and capture complete. ', 'wc-gateway-securenet');

				} else {
					$trans_message = __('SecureNet payment complete.', 'wc-gateway-securenet');
				}

				$order->add_order_note( $trans_message
						. ' (Reason Code: ' . $response_object->TRANSACTIONRESPONSE->REASON_CODE
						. '| RESPONSE_REASON_CODE: '. $response_object->TRANSACTIONRESPONSE->RESPONSE_REASON_CODE
						. '| RESPONSE_REASON_TEXT: '. $response_object->TRANSACTIONRESPONSE->RESPONSE_REASON_TEXT
						. '| AUTHCODE: '. $response_object->TRANSACTIONRESPONSE->AUTHCODE
						. '| AUTHORIZEDAMOUNT: '. $response_object->TRANSACTIONRESPONSE->AUTHORIZEDAMOUNT
						. '| BATCHID: '. $response_object->TRANSACTIONRESPONSE->BATCHID
						. '| TRANSACTIONID: '. $response_object->TRANSACTIONRESPONSE->TRANSACTIONID
						. '| TRANSACTIONDATETIME: '. $response_object->TRANSACTIONRESPONSE->TRANSACTIONDATETIME
						. ')' );
				$order->payment_complete();

				WC()->cart->empty_cart();

				// Empty awaiting payment session
				unset( WC()->session->order_awaiting_payment );

				// Return thank you redirect
				return array(
					'result' 	=> 'success',
					'redirect'	=> $this->get_return_url( $order )
				);

			} elseif ( $response_object->TRANSACTIONRESPONSE->RESPONSE_CODE == 2 ) {
				// Decline

				$this->send_debugging_email( "SecureNet Decline:\n"
						. ' (Reason Code: ' . $response_object->TRANSACTIONRESPONSE->REASON_CODE
						. '| RESPONSE_REASON_CODE: '. $response_object->TRANSACTIONRESPONSE->RESPONSE_REASON_CODE
						. '| RESPONSE_REASON_TEXT: '. $response_object->TRANSACTIONRESPONSE->RESPONSE_REASON_TEXT
						. '| AUTHCODE: '. $response_object->TRANSACTIONRESPONSE->AUTHCODE
						. '| AUTHORIZEDAMOUNT: '. $response_object->TRANSACTIONRESPONSE->AUTHORIZEDAMOUNT
						. '| BATCHID: '. $response_object->TRANSACTIONRESPONSE->BATCHID
						. '| TRANSACTIONID: '. $response_object->TRANSACTIONRESPONSE->TRANSACTIONID
						. '| TRANSACTIONDATETIME: '. $response_object->TRANSACTIONRESPONSE->TRANSACTIONDATETIME
						. ')' );

				$decline_note = 'SecureNet payment declined'
						. ' (Reason Code: ' . $response_object->TRANSACTIONRESPONSE->REASON_CODE
						. '| RESPONSE_REASON_CODE: '. $response_object->TRANSACTIONRESPONSE->RESPONSE_REASON_CODE
						. '| RESPONSE_REASON_TEXT: '. $response_object->TRANSACTIONRESPONSE->RESPONSE_REASON_TEXT
						. '| AUTHCODE: '. $response_object->TRANSACTIONRESPONSE->AUTHCODE
						. '| AUTHORIZEDAMOUNT: '. $response_object->TRANSACTIONRESPONSE->AUTHORIZEDAMOUNT
						. '| BATCHID: '. $response_object->TRANSACTIONRESPONSE->BATCHID
						. '| TRANSACTIONID: '. $response_object->TRANSACTIONRESPONSE->TRANSACTIONID
						. '| TRANSACTIONDATETIME: '. $response_object->TRANSACTIONRESPONSE->TRANSACTIONDATETIME
						. ')';

				$order->add_order_note( $decline_note );

				$this->debug(__('The payment has been declined. We can not process your payment at this time: ' . $response_object->TRANSACTIONRESPONSE->RESPONSE_REASON_TEXT, 'wc-gateway-securenet'), 'error');

			} else {
				// Error / Invalid Data
				$this->send_debugging_email( "SecureNet ERROR:\n"
						. ' (Reason Code: ' . $response_object->TRANSACTIONRESPONSE->REASON_CODE
						. '| RESPONSE_REASON_CODE: '. $response_object->TRANSACTIONRESPONSE->RESPONSE_REASON_CODE
						. '| RESPONSE_REASON_TEXT: '. $response_object->TRANSACTIONRESPONSE->RESPONSE_REASON_TEXT
						. '| AUTHCODE: '. $response_object->TRANSACTIONRESPONSE->AUTHCODE
						. '| AUTHORIZEDAMOUNT: '. $response_object->TRANSACTIONRESPONSE->AUTHORIZEDAMOUNT
						. '| BATCHID: '. $response_object->TRANSACTIONRESPONSE->BATCHID
						. '| TRANSACTIONID: '. $response_object->TRANSACTIONRESPONSE->TRANSACTIONID
						. '| TRANSACTIONDATETIME: '. $response_object->TRANSACTIONRESPONSE->TRANSACTIONDATETIME
						. ')' );

				$cancel_note = 'SecureNet payment failed'
						. ' (Reason Code: ' . $response_object->TRANSACTIONRESPONSE->REASON_CODE
						. '| RESPONSE_REASON_CODE: '. $response_object->TRANSACTIONRESPONSE->RESPONSE_REASON_CODE
						. '| RESPONSE_REASON_TEXT: '. $response_object->TRANSACTIONRESPONSE->RESPONSE_REASON_TEXT
						. '| AUTHCODE: '. $response_object->TRANSACTIONRESPONSE->AUTHCODE
						. '| AUTHORIZEDAMOUNT: '. $response_object->TRANSACTIONRESPONSE->AUTHORIZEDAMOUNT
						. '| BATCHID: '. $response_object->TRANSACTIONRESPONSE->BATCHID
						. '| TRANSACTIONID: '. $response_object->TRANSACTIONRESPONSE->TRANSACTIONID
						. '| TRANSACTIONDATETIME: '. $response_object->TRANSACTIONRESPONSE->TRANSACTIONDATETIME
						. ')';

				$order->add_order_note( $cancel_note );

				$this->debug(__('Payment error. We can not process your payment at this time.', 'wc-gateway-securenet'), 'error');
			}

		}

		/**
		 * validate_fields function.
		 *
		 * @access public
		 * @return bool
		 */
		public function validate_fields() {

			$cardNumber = $this->get_post('securenet_ccnum');
			$cardCSC = $this->get_post('securenet_cvv');
			$cardExpirationMonth = $this->get_post('securenet_expmonth');
			$cardExpirationYear = '20' . $this->get_post('securenet_expyear');

			//check security code
			if (!ctype_digit($cardCSC)) {
				$this->debug(__('Card security code is invalid (only digits are allowed)', 'wc-gateway-securenet'), 'error');
				return false;
			}

			//check expiration data
			$currentYear = date('Y');

			if (!ctype_digit($cardExpirationMonth) || !ctype_digit($cardExpirationYear) ||
				$cardExpirationMonth > 12 ||
				$cardExpirationMonth < 1 ||
				$cardExpirationYear < $currentYear ||
				$cardExpirationYear > $currentYear + 20
			) {
				$this->debug(__('Card expiration date is invalid', 'wc-gateway-securenet'), 'error');
				return false;
			}

			//check card number
			$cardNumber = str_replace(array(' ', '-'), '', $cardNumber);

			if (empty($cardNumber) || !ctype_digit($cardNumber)) {
				$this->debug(__('Card number is invalid', 'wc-gateway-securenet'), 'error');
				return false;
			}


			return true;
		}

		/**
		 * Get post data if set
		 **/
		private function get_post($name) {
			if (isset($_POST[$name])) {
				return $_POST[$name];
			}
			return null;
		}

		/**
		 * Format Request as XML (recursive)
		 **/
		private function xmlize($request)
		{
			$xml = '';

			ksort($request); // At one time, perhaps still, the items in the XML needed to be alphabetized. This does that.

			foreach($request as $k => $v) // For each root key of the array structure,
			{
				if(is_int($k))
				{
					$xml .= $this->xmlize($v);
				}
				elseif(is_array($v)) // if the value is an array,
				{
					$xml .= '<'.$k.'>';
					$xml .= $this->xmlize($v); // walk that new array like the first;
					$xml .= '</'.$k.'>';
				}
				elseif(is_bool($v) OR $v === "" OR is_null($v)) // however, if it is a bool value or NULL,
				{
					$xml .= '<'.$k.' i:nil="true" />'; // write out the funny bool i:nil value.
				}
				else // Otherwise, and these should all be strings, but it will accept ints or floats as well,
				{
					$xml .= '<'.$k.'>'.$v.'</'.$k.'>'; // just write the value into a standard tag structure.
				}
			}
			return $xml;
		}

		/**
		 * Output a message or error
		 * @param  string $message
		 * @param  string $type
		 */
		public function debug( $message, $type = 'notice' ) {
			wc_add_notice( $message, $type );
		}

 		/**
		 * Send debugging email
		 **/
		private function send_debugging_email( $debug ) {

			if ($this->debugon!='yes') return; // Debug must be enabled
			if (!$this->debugrecipient) return; // Recipient needed

			// Send the email
			wp_mail( $this->debugrecipient, __('SecureNet Debug', 'wc-gateway-securenet'), $debug );

		}
	}

	/**
	 * Plugin page links
	 */
	function wc_securenet_plugin_links( $links ) {

		$plugin_links = array(
			'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=wc_gateway_securenet' ) . '">' . __( 'Settings', 'wc-gateway-securenet' ) . '</a>',
			'<a href="http://www.woothemes.com/support">' . __( 'Support', 'wc-gateway-securenet' ) . '</a>',
			'<a href="http://docs.woothemes.com/document/securenet-payment-gateway/">' . __( 'Docs', 'wc-gateway-securenet' ) . '</a>',
		);

		return array_merge( $plugin_links, $links );
	}

	add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'wc_securenet_plugin_links' );

	/**
	 * Add the gateway to woocommerce
	 **/
	function add_securenet_gateway( $methods ) {
		$methods[] = 'WC_Gateway_Securenet'; return $methods;
	}

	add_filter('woocommerce_payment_gateways', 'add_securenet_gateway' );
}
